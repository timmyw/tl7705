# Reset control with a tl7705

After using the MAX705 and MAX708 for a while as supervisory ICs, I
wanted to try out the TL7705 equivalent.

They are very similar circuits, the biggest difference being the
TL7705 requires external capacitors to manage the reset signal time
length.

My requirements are pretty simple - a manual reset switch (that I
didn't need to debounce), and an active reset signal of around 200ms.
Having both an active low and active high reset signal is a bonus.

This is a very simple circuit which gives a reset signal with length
(approx) 270ms.  It is the capacitor between pin 3 and ground that
determines the timings.  For a 200ms active reset signal, I had
calculated I need a 15 uF capacitor, but the closest I have is 22 uF,
hence the 276 ms.

![schema](schema-v1.png "Schema v1")
